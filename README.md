# villages

This is a modpack for minetest that produces villages, complete with people and traders, 
of many kinds but the repo for complete mod pack was never build so i made one here

## Information

This mod pack builds around those mods:

| mod included      | observations                                  | forum address |
|-------------------|-----------------------------------------------|---------------|
| mg_villages       | creates teh villages and managed the features |  https://forum.minetest.net/viewtopic.php?f=9&t=13588 |
| handle_schematics | which is used to actually place the houses    | https://forum.minetest.net/viewtopic.php?f=9&t=13116  |
| cottages          | medieval, lumberjack and more village types   | https://forum.minetest.net/viewtopic.php?t=5120 |
| moresnow          | for nicer snow on roofs, fences and the like  | https://forum.minetest.net/viewtopic.php?f=9&t=9811 |
| mobf_trader       | people as traders inside the villages         | https://forum.minetest.net/viewtopic.php?t=9013 |
| village_canadian  | canadian style by LadyMcBest: Mauvebics MM2.6 modpack | https://github.com/Sokomine/village_canadian |
| village_gambit    | built by Gambit for the peaceful_npc mod      | https://github.com/Sokomine/village_gambit |
| village_modern_houses | Modern Houses Up for Grabs! adapted for   | https://forum.minetest.net/viewtopic.php?f=12&t=10705 |
| village_ruins     | bunkeers, cabinets, churs or towers in ruins  | https://forum.minetest.net/viewtopic.php?f=5&t=13297 |
| village_sandcity  | with sandmobs that we all so often kill in passing | https://github.com/Sokomine/village_sandcity |
| village_towntest  | from Cornernotes towntest mod adds type cornernote | https://forum.minetest.net/viewtopic.php?t=3223 |

The individual village_* mods are also intended as an example of how you can create your own villages

* Original mod pack was released around 2016/2018 at https://forum.minetest.net/viewtopic.php?f=9&t=13588
* Original wiki as at https://github.com/Sokomine/mg_villages/wiki
* Wiki Git: https://github.com/Sokomine/mg_villages.wiki.git

## Technical info

The mod is based on (and derived from)
Nores [experimental mg mapgen](https://forum.minetest.net/viewtopic.php?t=7263).
It does not depend on any particular mapgen.
You can create your own village types based on your own.

#### Dependences

Required 

* default
* farming
* stairs

Optional

* homedecor
* intllib
* trees
* wool
* moreblocks

#### Quick start

but the complete directory as `villages` name, and that's all, check settings at settings.

A modpack containing all the diffrent village types and mods required for a quick start
can be found in the original [villages modpack](https://forum.minetest.net/viewtopic.php?f=9&t=13589)
introduced on the Minetest forum. This reposiroty included all but highly modified for MinenuX minetest.

## License

Sokomine and ohers
